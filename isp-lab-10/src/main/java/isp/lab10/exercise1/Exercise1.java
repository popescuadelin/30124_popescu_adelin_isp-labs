package isp.lab10.exercise1;

public class Exercise1 {

    public static void main(String[] args) throws InterruptedException {
        AtcCommand atcCommand1=new AtcCommand();
        atcCommand1.setCommand("TAKEOFF {10] 11");
        Aircraft aircraft = new Aircraft("10");
        aircraft.recieveAtcMessage(atcCommand1);
        AtcCommand atcCommand2=new AtcCommand();
        atcCommand2.setCommand("TAKEOFF {11} 4");
        Aircraft aircraft1 = new Aircraft("11");
        aircraft1.recieveAtcMessage(atcCommand2);
        aircraft.start();
        aircraft1.start();
    }
}
