package isp.lab10.exercise1;

import java.util.Locale;
import java.util.Scanner;

public class AtcCommand {
    private String  command;
    private String id;
    private int altitude;
    Scanner scanner = new Scanner(System.in);
    AtcCommand(){}

    public void setCommand(String command) {
        String[] text = command.split("\\W+");
        this.command = text[0].toUpperCase();

        id = text[1];
        if(text.length==3){
            altitude=Integer.parseInt(text[2]);
            //System.out.println(this.command+this.id+this.altitude);
        }
        //System.out.println(this.command+this.id);
    }

    public int getAltitude() {
        return altitude;
    }

    public String getCommand() {
        return command;
    }

    public String getId() {
        return id;
    }
}
