package isp.lab10.exercise1;

public enum AircraftState {
    ONSTAND,TAKINGOFF,ASCENDING,CRUISING,TAXING,DESCENDING
}
