package isp.lab10.exercise1;

public class Aircraft extends Thread{
    private final String id;
    private int altitude=0;
    AtcCommand command;
    AircraftState aircraftState = AircraftState.ONSTAND;
    Aircraft(String id){
        this.id=id;
    }
    public void recieveAtcMessage(AtcCommand command){
        this.command=command;
//        if(command.getCommand().equals("TAKEOFF")){
//            aircraftState=AircraftState.TAXING;
//            System.out.println("aircraft " + command.getId()+ " goes thru taxing");
//            try{
//                Thread.sleep(10000);
//                }
//            catch (InterruptedException e){}
//            aircraftState=AircraftState.TAKINGOFF;
//            System.out.println("aircraft " + command.getId()+ " is taking off");
//            try{
//                Thread.sleep(5000);
//            }
//            catch (InterruptedException e){}
//            aircraftState=AircraftState.ASCENDING;
//            System.out.println("aircraft " + command.getId()+ " is ascending");
//           while (altitude<command.getAltitude()) {
//               try {
//                   Thread.sleep(10000);
//                   this.altitude++;
//                   System.out.println("Altitude: " + altitude);
//               } catch (InterruptedException e) {
//               }
//           }
//           aircraftState = AircraftState.CRUISING;
//            System.out.println("aircraft " + command.getId()+ " is cruising");
//
//        }
    }
    @Override
    public void run(){
        if(command.getCommand().equals("TAKEOFF")){
            aircraftState=AircraftState.TAXING;
            System.out.println("aircraft " + command.getId()+ " goes thru taxing");
            try{
                Thread.sleep(2000);
            }
            catch (InterruptedException e){};
            aircraftState=AircraftState.TAKINGOFF;
            System.out.println("aircraft " + command.getId()+ " is taking off");
            try{
                Thread.sleep(1000);
            }
            catch (InterruptedException e){};
            aircraftState=AircraftState.ASCENDING;
            System.out.println("aircraft " + command.getId()+ " is ascending");
            while (altitude<command.getAltitude()) {
                try {
                    Thread.sleep(2000);
                    this.altitude++;
                    System.out.println("Altitude: " + altitude);
                } catch (InterruptedException e) {
                };
            }
            aircraftState = AircraftState.CRUISING;
            System.out.println("aircraft " + command.getId()+ " is cruising");

        }else if(command.getCommand().equals("LAND") && aircraftState==AircraftState.CRUISING){
            System.out.println("aircraft " + command.getId()+ " is ascending");
            aircraftState=AircraftState.DESCENDING;
            while (altitude>0) {
                try {
                    Thread.sleep(2000);
                    this.altitude--;
                    System.out.println("Altitude: " + altitude);
                } catch (InterruptedException e) {
                };
            }
            System.out.println("aircraft " + command.getId()+ " has landed");
            aircraftState=AircraftState.ONSTAND;
        };
    }
    public String getAircraftId(){
        return id;
    }

    public AircraftState getAircraftState() {
        return aircraftState;
    }

    public int getPlaneAltitude() {
        return altitude;
    }
}
