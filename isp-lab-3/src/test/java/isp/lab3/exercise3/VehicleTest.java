package isp.lab3.exercise3;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class VehicleTest {
    @Test
    public void testTestToString() {
        String testString = "Dacia(Logan) speed 150 fuel type B";
        Vehicle logan = new Vehicle("Dacia","Logan",150,'B');
        assertEquals(testString,logan.toString());
    }
}