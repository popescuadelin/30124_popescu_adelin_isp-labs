package isp.lab3.exercise5;

import junit.framework.TestCase;
import org.junit.Test;

import java.util.Arrays;

public class VendingMachineTest extends TestCase {
    @Test
    public void testDisplayProducts(){
        VendingMachine vendingMachineTest1 = new VendingMachine();
        VendingMachine vendingMachineTest2 = new VendingMachine();

        assertEquals(true, Arrays.equals(vendingMachineTest1.displayProducts(), vendingMachineTest1.displayProducts()));
    }
    @Test
    public void testInsertCoin(){
        VendingMachine vendingMachineTest = new VendingMachine();
        assertEquals("shoud return the right credit",20,vendingMachineTest.insertCoin(20));
    }
    @Test
    public void testSelectProduct(){
        VendingMachine vendingMachineTest = new VendingMachine();
        vendingMachineTest.insertCoin(10);
        assertEquals("expects product id","product5",vendingMachineTest.selectProduct(5));
    }

}