package isp.lab3.exercise4;

import junit.framework.TestCase;
import org.junit.Test;

public class MyPointTest extends TestCase {
    @Test
    public void testDistance(){
        MyPoint point0 = new MyPoint(0,0,0);
        assertEquals("calc distance between two points",1.4142135623730951,point0.distance(1,1,0));
    }

}