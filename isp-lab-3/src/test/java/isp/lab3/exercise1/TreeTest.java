package isp.lab3.exercise1;

import junit.framework.TestCase;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TreeTest {
    @Test
    public void testGrow(){
        Tree testTree = new Tree();
        assertEquals("should expect 20",20,testTree.grow(5));
    }
    @Test
    public void testString(){
        Tree testTree = new Tree();
        String testString = "the height of the tree is: 15";
        assertEquals(testString, testTree.toString());
    }
}