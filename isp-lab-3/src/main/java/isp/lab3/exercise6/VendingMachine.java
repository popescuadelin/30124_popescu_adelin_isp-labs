package isp.lab3.exercise6;

import java.util.Scanner;

public  class VendingMachine {
    private static int credit;
    private static String[] product=new String[10];
    private VendingMachine(){
        credit=0;
        for(int i=0;i<10;i++){
            product[i]="product"+i;
        }
    }
    Scanner keyboard=new Scanner(System.in);
    public static void displayProducts(){
        for (int i=0;i<10;i++){
            System.out.print(product[i]+",(Id:"+i+"), ");
        }
        System.out.println();
    }
    public static void insertCoin(int coin){
        credit+=coin;
    }
    public static String selectProduct(int Id){
        if(Id>=0 && Id<10 && credit>0){
            credit--;
            return product[Id];
        }
        else return "error";
    }
    public static void displayCredit(){
        System.out.println("Credit: "+credit);
    }
    public void userMenu(){
        System.out.println("Products: ");
        displayProducts();
        displayCredit();
        System.out.println("insert coin: ");
        insertCoin(keyboard.nextInt());
        displayCredit();
        System.out.println("Select product: ");
        String prod=selectProduct(keyboard.nextInt());
        while(prod.equals("error"))
        {System.out.println("product selected: "+prod);
            System.out.println("Select product: ");
            prod=selectProduct(keyboard.nextInt());}
        System.out.println("product selected: "+prod);
        displayCredit();
    }

}
