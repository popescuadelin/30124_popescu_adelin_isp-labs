package isp.lab3.exercise3;

import java.util.Objects;

public class Vehicle {
    private String model,type;
    private int speed;
    private char fuelType;
    static int cars=0;
    public Vehicle(String model,String type,int speed,char fuelType){
        cars++;
        this.model=model;
        this.fuelType=fuelType;
        this.type=type;
        this.speed=speed;
    }
    public void setModel(String model){
        this.model=model;
    }
    public void setType(String type){
        this.type=type;
    }
    public void setSpeed(int speed){
        this.speed=speed;
    }
    public void setFuelType(char fuelType){
        this.fuelType=fuelType;
    }

    public char getFuelType() {
        return fuelType;
    }

    public String getModel() {
        return model;
    }
    public String getType(){
        return type;
    }
    public int getSpeed(){
        return speed;
    }
    public String toString(){
        String s = model + '(' + type + ')' + " speed " + speed + " fuel type " + fuelType;
        return s;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Vehicle)) return false;
        Vehicle vehicle = (Vehicle) o;
        return getSpeed() == vehicle.getSpeed() && getFuelType() == vehicle.getFuelType() && getModel().equals(vehicle.getModel()) && getType().equals(vehicle.getType());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getModel(), getType(), getSpeed(), getFuelType());
    }

    public static int getCars() {
        return cars;
    }
}
