package isp.lab3.exercise3;

public class Exercise3 {
    public static void main(String[] args){
        Vehicle car1 = new Vehicle("VW", "passat",210,'D');
        Vehicle car2 = new Vehicle("Audi","A4",300,'B');
        Vehicle car3 = new Vehicle("VW", "passat",210,'D');
        System.out.println("car 1 fuel type is:"+car1.getFuelType());
        car2.setType("RS6");
        System.out.println("Car 2 type:"+car2.getType());
        boolean a=(car1.equals(car3));
        System.out.println("Car 1 este egal cu car 3: "+a);
        System.out.println("Numarul de masini prezente este "+Vehicle.getCars());

    }
}
