package isp.lab3.exercise2;

public class RectangleType {
    int x,y;
    Rectangle rectangle=new Rectangle();
    public RectangleType(){
        x=0;y=0;
    }
    public RectangleType(int x,int y){
        this.x=x;
        this.y=y;
    }

    public int getX() {
        return x;
    }
    public int getY(){
        return y;
    }
}
