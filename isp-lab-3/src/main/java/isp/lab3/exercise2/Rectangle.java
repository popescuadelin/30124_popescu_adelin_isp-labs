package isp.lab3.exercise2;

public class Rectangle {
    private int length=2,width=1;
    String color="red";
    void Rectangle(int length, int width){
        this.length=length;
        this.width=width;
    }
    void Rectangle(int length,int width,String color){
        this.length=length;
        this.width=width;
        this.color=color;
    }
    public int getLength(){
        return length;
    }
    public int getWidth(){
        return width;
    }
    public String getColor(){
        return color;
    }
    public int getPerimeter(){
        return length*2+width*2;
    }
    public int getArea(){
        return width*length;
    }


}
