package isp.lab3.exercise2;

import isp.lab3.exercise1.Tree;

public class Exercise2 {
    public static void main(String[] args){
        RectangleType rectangle = new RectangleType(1,2);
        int area=rectangle.rectangle.getArea();
        int perimeter = rectangle.rectangle.getPerimeter();

        int x=rectangle.getX();
        int y = rectangle.getY();
        System.out.println(area +" " + perimeter  +" "+x+" "+y);
    }

}
