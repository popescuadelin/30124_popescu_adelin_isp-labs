package isp.lab3.exercise1;

public class Tree {
    int height;
    public Tree(){
        height=15;
    }
    public int grow(int meters){
        if(meters>=1)
            height+=meters;
        return height;
    }
    public String toString(){
        return "the height of the tree is: "+ height;
    }

}
