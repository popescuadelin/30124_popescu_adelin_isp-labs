package isp.lab4.exercise4;

import isp.lab4.exercise1.TemperatureSensor;
import isp.lab4.exercise2.FireAlarm;
import junit.framework.TestCase;
import org.junit.Test;

public class ControlerTest extends TestCase {
    @Test
    public void testControlSte() {
        TemperatureSensor[] temperatureSensors = new TemperatureSensor[3];
        temperatureSensors[0]=new TemperatureSensor(40,"bujii");
        temperatureSensors[1]=new TemperatureSensor(20,"scaun");
        temperatureSensors[2]=new TemperatureSensor(1000,"microprocesor");
        FireAlarm fireAlarmTest = new FireAlarm(false);
        Controler controlerTest = new Controler(temperatureSensors,fireAlarmTest);
        assertEquals(true,controlerTest.controlStep());
    }

}