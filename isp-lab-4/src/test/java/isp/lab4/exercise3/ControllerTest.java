package isp.lab4.exercise3;

import isp.lab4.exercise1.TemperatureSensor;
import isp.lab4.exercise2.FireAlarm;
import junit.framework.TestCase;
import org.junit.Test;

public class ControllerTest extends TestCase {
    @Test
    public void testControlStep(){
        TemperatureSensor temperatureSensorTest= new TemperatureSensor(100,"engine");
        FireAlarm fireAlarmTest = new FireAlarm(false);
        Controller controllerTest = new Controller(temperatureSensorTest,fireAlarmTest);
        assertEquals(true,controllerTest.controlStep());
    }

}