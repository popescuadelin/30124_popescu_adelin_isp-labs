package isp.lab4.exercise2;

import junit.framework.TestCase;
import org.junit.Test;

public class FireAlarmTest extends TestCase {
    @Test
    public void testIsActive(){
        FireAlarm fireAlarmTest = new FireAlarm(false);
        assertEquals(false,fireAlarmTest.isActive());
    }
    @Test
    public void testSetActive(){
        FireAlarm fireAlarmTest = new FireAlarm(false);
        assertEquals(true,fireAlarmTest.setActive(true));
    }
    @Test
    public void testToString(){
        FireAlarm fireAlarmTest1 = new FireAlarm(false);
        FireAlarm fireAlarmTest2 = new FireAlarm(true);
        assertEquals(false,fireAlarmTest1.toString().equals(fireAlarmTest2.toString()));

    }
}