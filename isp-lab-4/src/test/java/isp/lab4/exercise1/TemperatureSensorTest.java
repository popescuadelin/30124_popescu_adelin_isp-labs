package isp.lab4.exercise1;

import junit.framework.TestCase;
import org.junit.Test;

public class TemperatureSensorTest extends TestCase {
    @Test
    public void testGetValue(){
        TemperatureSensor temperatureSensorTest = new TemperatureSensor();
        assertEquals(0,temperatureSensorTest.getValue());
    }
    @Test
    public void testGetLocation(){
        TemperatureSensor temperatureSensorTest = new TemperatureSensor(0,"hota");
        assertEquals("hota",temperatureSensorTest.getLocation());
    }
    @Test
    public void testString(){
        TemperatureSensor temperatureSensorTest1 = new TemperatureSensor();
        TemperatureSensor temperatureSensorTest2 = new TemperatureSensor();
        assertEquals(true,temperatureSensorTest1.toString().equals(temperatureSensorTest2.toString()));
    }
}