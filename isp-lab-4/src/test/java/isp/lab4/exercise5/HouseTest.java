package isp.lab4.exercise5;

import junit.framework.TestCase;
import org.junit.Test;

public class HouseTest extends TestCase {
    @Test
    public void test1() {
        Controler controler1 = new Controler();
        Controler controler2 = new Controler();
        House houseTest1 = new House(controler1);
        House houseTest2 = new House(controler2);
        assertEquals(false,houseTest1.equals(houseTest2));
    }

}