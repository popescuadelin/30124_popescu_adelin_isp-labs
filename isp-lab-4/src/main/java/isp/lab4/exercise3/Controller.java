package isp.lab4.exercise3;

import isp.lab4.exercise1.TemperatureSensor;
import isp.lab4.exercise2.FireAlarm;

public class Controller {
    TemperatureSensor temperatureSensor=new TemperatureSensor();
    FireAlarm fireAlarm = new FireAlarm(true);
    public Controller(TemperatureSensor temperatureSensor,FireAlarm fireAlarm){
        this.temperatureSensor=temperatureSensor;
        this.fireAlarm=fireAlarm;
    }
    public boolean controlStep(){
        if(temperatureSensor.getValue()>50)
            fireAlarm.setActive(true);
        if(fireAlarm.isActive())
        {   System.out.println("Fire alarm started");
            return fireAlarm.isActive();}
        else{
            System.out.println("Fire alarm not started");
            return fireAlarm.isActive();}
    }


}
