package isp.lab4.exercise3;

import isp.lab4.exercise1.TemperatureSensor;
import isp.lab4.exercise2.FireAlarm;
import isp.lab4.exercise3.Controller;

public class ControllerTest {
    TemperatureSensor temperatureSensor1=new TemperatureSensor(10,"scaun");
    FireAlarm fireAlarm1=new FireAlarm(false);
    TemperatureSensor temperatureSensor2=new TemperatureSensor(100,"motor");
    FireAlarm fireAlarm2=new FireAlarm(false);
    public ControllerTest(){
        Controller controller1= new Controller(temperatureSensor1,fireAlarm1);
        controller1.controlStep();
        Controller controller2=new Controller(temperatureSensor2,fireAlarm2);
        controller2.controlStep();
    }
}
