package isp.lab4.exercise3;

import isp.lab4.exercise1.TemperatureSensor;
import isp.lab4.exercise2.FireAlarm;

public class Exercise3 {
    public static void main(String[] args){
        TemperatureSensor temperatureSensor=new TemperatureSensor(19,"motor");
        FireAlarm fireAlarm=new FireAlarm(false);
        Controller controller=new Controller(temperatureSensor,fireAlarm);
        controller.controlStep();
        new ControllerTest();
    }


}
