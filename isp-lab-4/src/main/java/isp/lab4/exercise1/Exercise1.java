package isp.lab4.exercise1;

//import isp.lab4.exercise0.CarAlarm; //NU ASA

public class Exercise1 {

    public static void main(String[] args) {
        //instantiati obiect
        TemperatureSensor t1= new TemperatureSensor();
        TemperatureSensor t2 = new TemperatureSensor(20,"bord");
        System.out.println("Temperatura masinii in interior este "+ t2.getValue()+" grade celsius");
        System.out.println(t1.toString());
    }
}
