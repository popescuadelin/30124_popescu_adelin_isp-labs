package isp.lab4.exercise6;

public class SalariedEmployee {
    double weeklySalary;
    public SalariedEmployee(){
        weeklySalary=400;
    }
    public SalariedEmployee(double weeklySalary) {
        this.weeklySalary=weeklySalary;
    }

    public void setWeeklySalary(double weeklySalary) {
        this.weeklySalary = weeklySalary;
    }

    public double getWeeklySalary() {
        return weeklySalary;
    }

    public double getPaymentAmount(){
        return 0.0;
    }
}
