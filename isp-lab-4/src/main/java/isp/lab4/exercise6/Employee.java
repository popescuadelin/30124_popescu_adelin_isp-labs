package isp.lab4.exercise6;

public class Employee {
    String firstName, lastName;
    ComissionEmployee comissionEmployee=new ComissionEmployee();
    HourlyEmployee hourlyEmployee=new HourlyEmployee();
    SalariedEmployee salariedEmployee= new SalariedEmployee();
    public Employee(){
        firstName="andrei";
        lastName="popescu";
    }
    public Employee(String lastName,String firstName,ComissionEmployee comissionEmployee,SalariedEmployee salariedEmployee){
        this.lastName=lastName;
        this.firstName=firstName;
        this.comissionEmployee=comissionEmployee;
        this.salariedEmployee=salariedEmployee;

    }
    public Employee(String lastName,String firstName,ComissionEmployee comissionEmployee,HourlyEmployee hourlyEmployee){
        this.lastName=lastName;
        this.firstName=firstName;
        this.hourlyEmployee=hourlyEmployee;
        this.comissionEmployee=comissionEmployee;

    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getFirstName(){
        return firstName;
    }
    public String getLastName(){
        return lastName;
    }
    public double getPaymentAmount(){
        return 0.0;
    }
}
