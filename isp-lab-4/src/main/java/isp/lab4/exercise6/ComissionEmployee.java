package isp.lab4.exercise6;

public class ComissionEmployee {
    double grossSales,comissionsSales;
    public ComissionEmployee(){
        grossSales=1000;
        comissionsSales=125;
    }
    public ComissionEmployee(double grossSales, double comissionsSales){
        this.comissionsSales=comissionsSales;
        this.grossSales=grossSales;
    }
        public double getPaymentAmount(){
            return 0.0;
        }

    public void setComissionsSales(double comissionsSales) {
        this.comissionsSales = comissionsSales;
    }

    public void setGrossSales(double grossSales) {
        this.grossSales = grossSales;
    }
    public double getGrossSales(){
        return grossSales;
    }

    public double getComissionsSales() {
        return comissionsSales;
    }
}
