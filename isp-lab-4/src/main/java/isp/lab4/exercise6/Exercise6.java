package isp.lab4.exercise6;

public class Exercise6 {
    public static void main(String[] args){
        double totalPayment=0;
        ComissionEmployee comissionEmployee =  new ComissionEmployee();
        HourlyEmployee hourlyEmployee = new HourlyEmployee();
        SalariedEmployee salariedEmployee = new SalariedEmployee();
        System.out.println(comissionEmployee.getPaymentAmount()+"  "+ hourlyEmployee.getPaymentAmount()+"  "+ salariedEmployee.getPaymentAmount());
        Employee[] employees=new Employee[6];
        for(int i=0;i<4;i++){
            if(i%2!=0)
            employees[i]=new Employee("Nume"+i,"Prenume"+i,comissionEmployee,salariedEmployee);
            else
                employees[i]=new Employee("Nume"+i,"Prenume"+i,comissionEmployee,hourlyEmployee);
        }
        for(int i=0;i<4;i++){
            totalPayment+=(employees[i].hourlyEmployee.getHours()*employees[i].hourlyEmployee.getWage());
            totalPayment+=(employees[i].salariedEmployee.getWeeklySalary()*4);
        }
        System.out.println(totalPayment);

    }
}
