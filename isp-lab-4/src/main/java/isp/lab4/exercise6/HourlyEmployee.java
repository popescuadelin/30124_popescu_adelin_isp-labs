package isp.lab4.exercise6;

public class HourlyEmployee {
    double wage,hours;
    public HourlyEmployee(){
        wage=30;
        hours=40;
    }
    public HourlyEmployee(double wage, double hours) {
        this.wage = wage;
        this.hours = hours;
    }

    public void setHours(double hours) {
        this.hours = hours;
    }

    public void setWage(double wage) {
        this.wage = wage;
    }

    public double getHours() {
        return hours;
    }

    public double getWage() {
        return wage;
    }

    public double getPaymentAmount(){
        return 0.0;
    }
}
