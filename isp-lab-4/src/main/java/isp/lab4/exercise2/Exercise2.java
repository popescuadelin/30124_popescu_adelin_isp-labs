package isp.lab4.exercise2;

public class Exercise2 {
    public static void main(String[] args){
        FireAlarm fireAlarm1 = new FireAlarm(false);
        System.out.println(fireAlarm1.toString());
        fireAlarm1.setActive(true);
        System.out.println(fireAlarm1.toString());
    }
}
