package isp.lab4.exercise5;



public class Controler {
    boolean fire=false;
    TemperatureSensor[] temperatureSensors=new TemperatureSensor[3];
    FireAlarm fireAlarm=new FireAlarm(false);
    public Controler(){
        temperatureSensors[0]=new TemperatureSensor();
        temperatureSensors[1]=new TemperatureSensor(40,"Aragaz");
        temperatureSensors[2]=new TemperatureSensor();
        fireAlarm=new FireAlarm(false);
    }
    public boolean controlStep(){
        for(int i=0;i<3;i++){
            if (temperatureSensors[i].getValue() > 50) {
                fire = true;
                fireAlarm.setActive(true);
                break;
            }
        }
        if(fire) {
            System.out.println("Fire alarm  started");
            return fireAlarm.isActive();
        }
        else {
            System.out.println("Fire alarm not started");
            return fireAlarm.isActive();
        }
    }
}