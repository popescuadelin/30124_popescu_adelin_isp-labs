package isp.lab4.exercise5;

public class FireAlarm {
    boolean active;
    public FireAlarm(boolean active){
        this.active=active;
    }
    public boolean isActive(){
        return active;
    }

    public boolean setActive(boolean active) {
        this.active = active;
        return this.active;
    }

    @Override
    public String toString() {
        return "FireAlarm{" +
                "active=" + active +
                '}';
    }
}
