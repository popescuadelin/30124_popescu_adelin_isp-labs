package isp.lab4.exercise4;

import isp.lab4.exercise1.TemperatureSensor;
import isp.lab4.exercise2.FireAlarm;

public class Controler {
    boolean fire=false;
    TemperatureSensor[] temperatureSensors=new TemperatureSensor[3];
    FireAlarm fireAlarm=new FireAlarm(false);
    public Controler(){ }
    public Controler(TemperatureSensor[] temperatureSensors,FireAlarm fireAlarm){
        this.temperatureSensors=temperatureSensors;
        this.fireAlarm=fireAlarm;
    }

    public boolean controlStep(){
        for(int i=0;i<3;i++){
            if(temperatureSensors[i].getValue()>50)
                fire=true;
        }
        if(fire) {
            System.out.println("Fire alarm  started");
            fireAlarm.setActive(true);
            return true;
        }
        else {
            System.out.println("Fire alarm not started");
            fireAlarm.setActive(false);
            return false;
        }
    }
}
