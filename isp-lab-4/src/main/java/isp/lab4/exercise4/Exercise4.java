package isp.lab4.exercise4;

import isp.lab4.exercise1.TemperatureSensor;
import isp.lab4.exercise2.FireAlarm;

public class Exercise4 {
    public static void main(String[] args){
        TemperatureSensor[] temperatureSensors=new TemperatureSensor[3];
        temperatureSensors[0]=new TemperatureSensor(100,"bujii");
        temperatureSensors[1]=new TemperatureSensor(80,"scaun");
        temperatureSensors[2]=new TemperatureSensor(1000,"microprocesor");

        FireAlarm fireAlarm=new FireAlarm(false);
        Controler controler=new Controler(temperatureSensors,fireAlarm);
        controler.controlStep();
    }
}
