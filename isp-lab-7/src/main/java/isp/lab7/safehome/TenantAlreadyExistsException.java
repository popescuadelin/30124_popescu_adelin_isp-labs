package isp.lab7.safehome;

public class TenantAlreadyExistsException extends Exception {

    public TenantAlreadyExistsException() {
    }

    public TenantAlreadyExistsException(String message) {
        super(message);
    }

    public TenantAlreadyExistsException(String message, Throwable cause) {
        super(message, cause);
    }

}
