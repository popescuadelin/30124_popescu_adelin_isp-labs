package isp.lab7.safehome;

public class Door {
    private DoorStatus status;

    public Door(DoorStatus status) {
        this.status = status;
    }

    public DoorStatus getStatus() {
        return status;
    }

    void lockDoor(){this.status=DoorStatus.CLOSE; }

    void unlockDoor(){this.status=DoorStatus.OPEN;}
}
