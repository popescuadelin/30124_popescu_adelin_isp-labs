package isp.lab7.safehome;

public class TooManyAttemptsException extends Exception {

    public TooManyAttemptsException() {
    }

    public TooManyAttemptsException(String message) {
        super(message);
    }

    public TooManyAttemptsException(String message, Throwable cause) {
        super(message, cause);
    }
}
