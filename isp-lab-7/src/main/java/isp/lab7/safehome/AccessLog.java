package isp.lab7.safehome;

import java.time.LocalDateTime;

public class AccessLog {
    private String operation,tenantName,errorMessage;
    private LocalDateTime dataTime;
    private DoorStatus doorStatus;

    public AccessLog(String operation, String tenantName, String errorMessage, LocalDateTime dataTime) {
        this.operation = operation;
        this.tenantName = tenantName;
        this.errorMessage = errorMessage;
        this.dataTime = dataTime;
    }
}
