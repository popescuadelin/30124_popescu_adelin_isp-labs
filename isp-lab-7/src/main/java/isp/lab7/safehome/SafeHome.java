package isp.lab7.safehome;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class SafeHome {

    public static void main(String[] args) throws Exception {

        Door door = new Door(DoorStatus.CLOSE);
        List<AccessLog> accessLogs = new ArrayList<>();
        accessLogs.add(new AccessLog("open","Radu","nu se poate sefu", LocalDateTime.now()));
        accessLogs.add(new AccessLog("open","Marius","sesam nu vrea",LocalDateTime.now()));
        DoorLockController lockController = new DoorLockController(door,accessLogs);
        lockController.addTenant("1234","Alex");
        lockController.addTenant("1234","Mircea");
        lockController.enterPin("1234");
        System.out.println(lockController.getAccessLogs());
    }
}
