package isp.lab7.safehome;

public class AccessPin {
    private String pin;

    public String getPin() {
        return pin;
    }

    public AccessPin(String pin) {
        this.pin = pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }
}
