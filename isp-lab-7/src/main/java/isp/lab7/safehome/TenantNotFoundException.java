package isp.lab7.safehome;

public class TenantNotFoundException extends Exception {

    public TenantNotFoundException() {
    }

    public TenantNotFoundException(String message) {
        super(message);
    }

    public TenantNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
