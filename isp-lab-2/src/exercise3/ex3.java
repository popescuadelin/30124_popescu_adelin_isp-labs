package exercise3;

import java.util.Scanner;

public class ex3 {
    static boolean checkPrime(int numberToCheck)
    {
        if(numberToCheck == 1) {
            return false;
        }
        for (int i = 2; i*i <= numberToCheck; i++) {
            if (numberToCheck % i == 0) {
                return false;
            }
        }
        return true;
    }
    static int sum(int l, int r)
    {
        int sum = 0,count =0;
        for (int i = r; i >= l; i--) {

            // Check for prime
            boolean isPrime = checkPrime(i);
            if (isPrime) {

                // Sum the prime number
                sum = sum + i;
                count++;
            }
        }
        return sum/count;
    }
    static int psum(int n){
        int s1=0, s2=0,count=2;
        while(n!=0){
            boolean isPrime = checkPrime(count);
            if(isPrime){
                s1+=count;
                if(s1%2==0)
                    s2=s1;
            }
            count++;
            n--;
        }
        return s2;
    }
    public static void main(String[] args){
        int a,b;
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Introduceti a si b:");
        a=keyboard.nextInt();
        b=keyboard.nextInt();
        int m=sum(a,b);
        System.out.println("Media aritmetica a numerelor prime de la "+a+" la "+b+" este:"+m);
        System.out.println("alegeti un nu numar de elemente prime: ");
        int n=keyboard.nextInt();
        System.out.print(psum(n));

    }
}
