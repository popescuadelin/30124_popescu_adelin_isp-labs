package isp.lab6.exercise1;
import java.util.Comparator;

public class CompareByLocation implements Comparator<SensorReading> {


        // Method
        // Sorting in ascending order of name
        public int compare(SensorReading a, SensorReading b)
        {

            return a.getLocation().compareTo(b.getLocation());
        }

}
