package isp.lab6.exercise1;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class SensorReadingList implements IReadingRepository{
    List<SensorReading> list=new ArrayList<SensorReading>();
    @Override
    public void addReading(SensorReading reading){
    this.list.add(reading);
    }

    @Override
    public double getAvarageValueByType(Type type, String location) {
        double avr=0;int i=0;
        for (SensorReading sensor :list){
            if(sensor.getType()==type && sensor.getLocation().equals(location)){
                i=i+1;
                avr=avr+sensor.getValue();
            }
        }
        return avr/i;
    }

    @Override
    public List<SensorReading> getReadingsByType(Type type) {
        List<SensorReading> returnList=new ArrayList<SensorReading>();
        for (SensorReading sensor :list){
            if(sensor.getType().equals(type))
                returnList.add(sensor);
        }
        return returnList;
    }

    @Override
    public void listSortedByLocation() {
        Collections.sort(list,new CompareByLocation());
    }

    @Override
    public void listSortedByValue() {
        for (int i =0;i<list.size();i++){
            list.get(i).getValue();
        }
    }

    @Override
    public List<SensorReading> findAllByLocationAndType(String location, Type type) {
        List<SensorReading> returnList=new ArrayList<SensorReading>();
        for(SensorReading i:list){
            if(i.getLocation().equals(location) && i.getType().equals(type))
                returnList.add(i);
        }
        return returnList;
    }
}
