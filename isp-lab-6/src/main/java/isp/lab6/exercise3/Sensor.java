package isp.lab6.exercise3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Sensor implements Comparator<SensorReading>{
    private SENSOR_TYPE type;
    private String id;
    List<SensorReading> sensorReadingsList = new ArrayList<>();
    Sensor(SENSOR_TYPE type,String id){
        this.id=id;
        this.type=type;
    }
    Sensor(SENSOR_TYPE type,String id,List<SensorReading> sensorReadingsList){
        this.id=id;
        this.type=type;
        this.sensorReadingsList=sensorReadingsList;
    }

    public List<SensorReading> getSensorReadingsSortedByDateAndTime(){
        Collections.sort(sensorReadingsList,SensorReading::compareTo);
        return sensorReadingsList;
    }
    public List<SensorReading> getSensorReadingsSortedByValue(){

        Collections.sort(sensorReadingsList,this::compare);
        return sensorReadingsList;
    }
    public boolean addSensorReading(SensorReading sr){
        sensorReadingsList.add(sr);
        return true;
    }

    public String getId() {
        return id;
    }

    public SENSOR_TYPE getType() {
        return type;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setType(SENSOR_TYPE type) {
        this.type = type;
    }
    @Override
        public int compare(SensorReading o1, SensorReading o2) {
            return o1.getValue()>=o2.getValue()?1:-1;
        }

    @Override
    public String toString() {
        return "Sensor{" +
                "type=" + type +
                ", id='" + id + '\'' +
                ", sensorReadingsList=" + sensorReadingsList +
                '}';
    }
}
