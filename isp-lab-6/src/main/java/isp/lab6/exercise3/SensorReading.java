package isp.lab6.exercise3;

import java.time.LocalDateTime;

public class SensorReading implements Comparable<SensorReading>{
    private LocalDateTime dateAndTime;
    private double value;
    SensorReading(double value, LocalDateTime dateAndTime) {
        this.value=value;
        this.dateAndTime=dateAndTime;
    }
    @Override
    public int compareTo(SensorReading o) {
        return o.getDateAndTime().compareTo(this.getDateAndTime());
    }

    public void setDateAndTime(LocalDateTime dateAndTime) {
        this.dateAndTime = dateAndTime;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public double getValue() {
        return value;
    }

    public LocalDateTime getDateAndTime() {
        return dateAndTime;
    }


}
