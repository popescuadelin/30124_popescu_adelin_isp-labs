package isp.lab6.exercise3;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class SensorsCluster {
    List<Sensor> sensorList = new ArrayList<>();
    SensorsCluster(){}
    SensorsCluster(List<Sensor> sensors){
        this.sensorList=sensors;
    }
    public Sensor addSensor(String sensorID,SENSOR_TYPE type){
        boolean exixts=false;
        for(Sensor sensor : sensorList){
            if(sensor.getId().equals(sensorID)){
                exixts=true;
            }
        }
        if(!exixts){
            Sensor sensor=new Sensor(type,sensorID);
            sensorList.add(sensor);
            return sensor;
        }
        return null;
    }
    public boolean writeSensorReading(String sensorID, double value, LocalDateTime dateTime){
       for(Sensor sensor : sensorList){
           if(sensor.getId().equals(sensorID)){
               sensor.addSensorReading(new SensorReading(value,dateTime));
               return true;
           }
       }
       return false;
    }
    public Sensor getSensorById(String sensorId){
        for(Sensor sensor : sensorList){
            if(sensor.getId().equals(sensorId)){
               System.out.println(sensor.getSensorReadingsSortedByDateAndTime());
               System.out.println(sensor.getSensorReadingsSortedByValue());
               return sensor;
            }
    }
        return null;
}
}
