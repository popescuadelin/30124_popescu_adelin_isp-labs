package isp.lab6.exercise3;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class exercise3 {
    public static void main(String[] args){
        List<Sensor> sensors = new ArrayList<>();
        sensors.add(new Sensor(SENSOR_TYPE.TEMPERATURE,"sensor1"));
        sensors.add(new Sensor(SENSOR_TYPE.HUMIDITY,"sensor2"));
        sensors.add(new Sensor(SENSOR_TYPE.PRESSURE,"sensor3"));
        SensorsCluster sensorsCluster = new SensorsCluster(sensors);
        System.out.println(sensorsCluster.addSensor("sensor4",SENSOR_TYPE.TEMPERATURE));
        SensorReading sensorReading = new SensorReading(100, LocalDateTime.of(2022,1,5,10,11));
        System.out.println(sensorReading.getDateAndTime());
        System.out.println(sensorsCluster.addSensor("sensor4",SENSOR_TYPE.TEMPERATURE));
    }
}
