package isp.lab6.exercise2;

import java.util.*;

public class EquipmentController {
    List<Equipment> list=new ArrayList<Equipment>();
    /**
     * Add new equipment to the list of equipments
     *
     * @param equipment - equipment to be added
     */
    public void addEquipment(final Equipment equipment) {
        list.add(equipment);

    }

    /**
     * Get current list of equipments
     *
     * @return list of equipments
     */
    public List<Equipment> getEquipments() {
        return  list;
    }

    /**
     * Get number of equipments
     *
     * @return number of equipments
     */
    public int getNumberOfEquipments() {
        return list.size();
    }

    /**
     * Group equipments by owner
     *
     * @return a dictionary where the key is the owner and value is represented by list of equipments he owns
     */
    public Map<String, List<Equipment>> getEquipmentsGroupedByOwner() {
        List<Equipment> fol=new ArrayList<Equipment>();
        List<Equipment> segment=new ArrayList<Equipment>();
        fol.addAll(list);
    Map< String,List<Equipment>> rezultat= new Hashtable<String,List<Equipment>>();

    while (!fol.isEmpty()){
        segment.clear();
    String s=fol.get(0).getOwner();
        for(Equipment i:fol){
        if(s.equals(i.getOwner()))
        {
            segment.add(i);
            fol.remove(i);
        }
    }
        rezultat.put(s,segment);
    }
    return rezultat;
    }

    /**
     * Remove a particular equipment from equipments list by serial number
     * @param serialNumber - unique serial number
     * @return deleted equipment instance or null if not found
     */
    public Equipment removeEquipmentBySerialNumber(final String serialNumber) {

        for(Equipment i:list){
        if(i.getSerialNumber().equals(serialNumber)){
            list.remove(i);
            return i;
        }
        }
        return null;
    }
}
