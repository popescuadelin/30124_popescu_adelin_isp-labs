import sun.security.action.OpenFileInputStreamAction;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.*;

public class NotePad extends JFrame implements ActionListener, WindowListener {

    JTextArea textArea= new JTextArea();
    File fnameContainer;
    public NotePad(){
        Font fn = new Font("Arial",Font.PLAIN,15);
        Container container= getContentPane();
        JMenuBar jmb=new JMenuBar();
        JMenu jMenu= new JMenu("File");
        JMenu jmedit = new JMenu("Edit");
        JMenu jhelp = new JMenu("Help");

        container.setLayout(new BorderLayout());
        JScrollPane sbrText = new JScrollPane(textArea);
        sbrText.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        sbrText.setVisible(true);

        textArea.setFont(fn);
        textArea.setLineWrap(true);
        textArea.setWrapStyleWord(true);

        container.add(sbrText);

        createMenuItem(jMenu,"New");
        createMenuItem(jMenu,"Open");
        createMenuItem(jMenu,"Save");
        jMenu.addSeparator();
        createMenuItem(jMenu,"Exit");

        createMenuItem(jmedit,"Cut");
        createMenuItem(jmedit,"Copy");
        createMenuItem(jmedit,"Paste");


        createMenuItem(jhelp,"About Notepad");

        jmb.add(jMenu);
        jmb.add(jmedit);
        jmb.add(jhelp);

        setJMenuBar(jmb);

        setIconImage(Toolkit.getDefaultToolkit().getImage("notepad.gif"));
        addWindowListener(this);
        setSize(500,500);
        setTitle("Untitled.txt - Notepad");
        setVisible(true);
    }
    public void createMenuItem(JMenu jm, String txt){
        JMenuItem jmi = new JMenuItem(txt) ;
        jmi.addActionListener(this);
        jm.add(jmi);
    }
    @Override
    public void actionPerformed(ActionEvent e) {
            JFileChooser jfc = new JFileChooser();
            if(e.getActionCommand().equals("New")){
                this.setTitle("Untitle.Txt - Notepad");
                textArea.setText("");
                fnameContainer = null;
            }else if(e.getActionCommand().equals("Open")){
                int ret = jfc.showDialog(null,"Open");
                if(ret==JFileChooser.APPROVE_OPTION){
                    try{
                        File fyl = jfc.getSelectedFile();
                        OpenFile(fyl.getAbsolutePath());
                        this.setTitle(fyl.getName()+" - Notepad");
                        fnameContainer = fyl;
                    }catch (IOException ev){ }
                }
            }else if(e.getActionCommand().equals("Save")){
                if(fnameContainer!=null){
                    jfc.setCurrentDirectory(fnameContainer);
                    jfc.setSelectedFile(fnameContainer);
                }else{
                    jfc.setSelectedFile(new File("Untitled.txt"));
                }
                int ret = jfc.showSaveDialog(null);
                if(ret== JFileChooser.APPROVE_OPTION){
                    try{
                        File fyl = jfc.getSelectedFile();
                        SaveFile(fyl.getAbsolutePath());
                        this.setTitle(fyl.getName()+" - -Notepad");
                        fnameContainer = fyl;
                    }catch (Exception ev){}
                }
            }else if(e.getActionCommand().equals("Exit")){
                Exiting();
            }else if(e.getActionCommand().equals("Copy")){
                textArea.copy();
            }else if(e.getActionCommand().equals("Paste")){
                textArea.paste();
            }else if(e.getActionCommand().equals("Cut")){
                textArea.cut();
            }else if(e.getActionCommand().equals("About Notepad")){
                JOptionPane.showMessageDialog(this,"Created by : Adelin","NotePad",JOptionPane.INFORMATION_MESSAGE);
            }
    }
    public void OpenFile(String fname) throws IOException{
        BufferedReader d = new BufferedReader(new InputStreamReader(new FileInputStream(fname)));
        String l;
        textArea.setText("");
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        while((l=d.readLine())!=null){
            textArea.setText(textArea.getText()+l+"\r\n");
        }
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        d.close();
    }
    public  void SaveFile(String fname) throws Exception{
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        DataOutputStream o = new DataOutputStream(new FileOutputStream(fname));
        o.writeBytes(textArea.getText());
        o.close();
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {
        Exiting();
    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }
    public void Exiting(){
        System.exit(0);
    }
}
