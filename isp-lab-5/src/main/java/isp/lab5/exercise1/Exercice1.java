package isp.lab5.exercise1;

public class Exercice1 {
    public static void main(String[] args){
        Card[] cards=new Card[10];
        cards[1]=new Card("1","1234");
        cards[1]=new Card("2","1234");
        cards[1]=new Card("3","1234");
        cards[1]=new Card("4","1234");
        cards[1]=new Card("5","1234");
        cards[1]=new Card("6","1234");
        cards[1]=new Card("7","1234");
        cards[1]=new Card("8","1234");
        cards[1]=new Card("9","1234");
        cards[1]=new Card("10","1234");
        Account[] accounts=new  Account[10];

        accounts[1]=new Account("Cristi",10000,cards[1]);
        accounts[2]=new Account("Mihai",20000,cards[2]);
        accounts[3]=new Account("Dan",30000,cards[3]);
        accounts[1]=new Account("Cristi",10000,cards[4]);
        accounts[1]=new Account("Rares",10000,cards[5]);
        accounts[1]=new Account("Mihail",10000,cards[6]);
        accounts[1]=new Account("Misu",10000,cards[7]);
        accounts[1]=new Account("Lidia",10000,cards[8]);
        accounts[1]=new Account("Radu",10000,cards[9]);
        Bank bank=new Bank(accounts);
        ATM atm=new ATM();
        atm.insertCard(cards[2],"1234");
       // atm.changePin("1234","1928");
        atm.removeCard();
    }
}
