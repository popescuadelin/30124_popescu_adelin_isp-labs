package isp.lab5.exercise1;

public abstract class Transaction {
    abstract String execute();
}
