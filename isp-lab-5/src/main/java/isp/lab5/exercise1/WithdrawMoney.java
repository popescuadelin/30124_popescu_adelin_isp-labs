package isp.lab5.exercise1;

public class WithdrawMoney extends Transaction {
    double amount;
    WithdrawMoney(double amount){
      this.amount=amount;
    }
    @Override
    public String  execute(){
            return "Withdraw complete \n"+amount+" were extracted";
    }
}
