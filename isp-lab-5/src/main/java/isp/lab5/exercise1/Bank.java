package isp.lab5.exercise1;

public class Bank {
    Account[] accounts=new Account[10];
    public Bank(Account[] accounts){
        this.accounts=accounts;
    }
    public String executeTransaction(Transaction transaction){
        return transaction.execute();
    }
    public Account getAccountByCardId(String cardId){
        for(int i=0;i<10;i++)
            if(accounts[i].equals(cardId))
                return accounts[i];
        return null;
    }
}
