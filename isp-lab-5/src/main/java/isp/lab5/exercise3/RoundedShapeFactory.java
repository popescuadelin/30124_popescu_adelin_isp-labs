package isp.lab5.exercise3;

public class RoundedShapeFactory extends AbstractShapeFactory {
    @Override
    Shape getShape(String type) {
        if ("roundedRectangle".equals(type)) {
            return new RoundedRectangle();
        }else  if ("roundedSquare".equals(type)) {
            return new RoundedSquare();
        }

        return null;
    }
}

