package isp.lab5.exercise3;

public  class  Exercise3{

    public static void main(String[] args) {
        AbstractShapeFactory roundedShapeFactory = ShapeFactoryProvider.getShapeFactory("rounded");
        AbstractShapeFactory normalShapeFactory = ShapeFactoryProvider.getShapeFactory("normal");
        AbstractShapeFactory roundedShapeFactory1 = ShapeFactoryProvider.getShapeFactory("rounded");
        AbstractShapeFactory normalShapeFactory1 = ShapeFactoryProvider.getShapeFactory("normal");

        // create instances
        Shape rectangle = roundedShapeFactory.getShape("roundedRectangle");
        Shape shape = normalShapeFactory.getShape("rectangle");
        Shape square = roundedShapeFactory1.getShape("roundedSquare");
        Shape shape1 = normalShapeFactory1.getShape("square");

        rectangle.draw();
        shape.draw();
        square.draw();
        shape1.draw();
    }
}