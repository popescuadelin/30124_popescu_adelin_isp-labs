package isp.lab5.exercise2;

interface Playable {
    void play();
}

class ColorVideo implements Playable {

    private String fileName;

    public ColorVideo(String fileName){
        this.fileName = fileName;
        loadFromDisk(fileName);
    }

    @Override
    public void play() {
        System.out.println("Play " + fileName);
    }

    private void loadFromDisk(String fileName){
        System.out.println("Loading video..." + fileName);
    }
}
class BlackAndWhiteVideo implements Playable{
    private String fileName;
    public BlackAndWhiteVideo(String fileName){
        this.fileName=fileName;
        loadFromDisk(fileName);
    }

    @Override
    public void play() {
        System.out.println("Play black and white video " + fileName);
    }

    private void loadFromDisk(String fileName){
        System.out.println("Loading video..." + fileName);
    }
}
class ProxyVideo implements Playable {

    private ColorVideo video;
    private BlackAndWhiteVideo oldVideo;
    private String fileName;
    private String videoType;

    public ProxyVideo(String fileName,String videoType){
        this.fileName = fileName;
        this.videoType=videoType;
    }

    @Override
    public void play() {
        if(videoType.equals("color")){
        if(video == null){
            video = new ColorVideo(fileName);
        }
        video.play();
    }
    else if(oldVideo==null){
        oldVideo = new BlackAndWhiteVideo(fileName);
        oldVideo.play();
    }}
}

public class Exercise2 {
    public static void main(String[] args){

    }
}
