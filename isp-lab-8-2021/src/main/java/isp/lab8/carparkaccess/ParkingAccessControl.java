/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isp.lab8.carparkaccess;

import java.time.format.SignStyle;
import java.util.ArrayList;

/**
 *
 * @author mihai.hulea
 */
public class ParkingAccessControl {

    public static final int MAX_CAPACITY = 5;
    
    private ArrayList<Car>  parkedCars = new ArrayList<>();
   
    private ArrayList<Car>  previousCars = new ArrayList<>(); 

    public void carEntry(Car car){
        //1. sa verific capacitatea
        System.out.println("The parking lot capacity is "+ MAX_CAPACITY);
        //2. daca capacitatea este depasita -> return
        if(parkedCars.size()>MAX_CAPACITY){
            System.out.println("The parking lot is full");
        }
        //3. verific daca nu cumva masina este deja in parcare, si daca este -> return
        if(parkedCars.contains(car)){
            System.out.println("The carr it's already here");
        }
        //4. daca masina nu este in parcare salvez obiectul de tip car in parkedCars
        else{
            parkedCars.add(car);
        }

        System.out.println("Car "+car+" is entering car park!");
    }
    
    public int carExit(String plateNumber){
        boolean parked = false;
        //1. cautam dupa plate number un Car in parkedCars
        for(Car car : parkedCars){
            if (car.getPlateNumber().equals(plateNumber)){

                //3. daca am gasit masina,
                //calculez timpul de asteptare in parcare, -> System.currentTimeMiliseconds()
                long parkedTime=System.currentTimeMillis()-car.getEntryTime();
                //calculez pretul,
                long price = parkedTime/1000;
                //sterg masina din parkedCars si o adaug in previousCars
                previousCars.add(car);
                parkedCars.remove(car);
                //returnez pretul
                System.out.println("Car with plate number "+plateNumber+" is exiting");
                return (int)price;
            }
        }
        //2. daca nu am gasit plateNumber -> return
        System.out.println("Car with plate number: "+plateNumber+" is not existing");
        return 0;
    }
    
    public void viewCurrentCars(){
        System.out.println("Display all parked cars.");
        System.out.println(parkedCars);
    }
    
    public void viewPastEntriesForCar(String plateNumber){
        System.out.println("Display all past entries for a car.");
        for(Car car : previousCars){
            if(car.getPlateNumber().equals(plateNumber)){
                System.out.println(car);
            }
        }
    }
    
    public void viewPastUniqueEntries(String plateNumber){
        System.out.println("Display all unique entries.");
        for(Car car : previousCars){
            if(car.getPlateNumber().equals(plateNumber)){
                System.out.println(car);
                break;
            }
        }
    }
    
}
