package isp.lab9.exercise2;

import isp.lab9.exercise1.Observer;

public class Sensor {
    private SENSOR_TYPE sensor_type;
    Controller controller = new Controller();
    private int value;
    Sensor(SENSOR_TYPE sensor_type){
        this.sensor_type=sensor_type;
        value=0;
    }
    void notifyController(Object event){
        controller.update(event);
    }
    void readSensor(){
        value = (int) (Math.random()*100);
        System.out.println(sensor_type + " value is: "+ value);
    }
    void changeValue(Observer observer){
        readSensor();
        notifyController("Value Changed");
    }

    public int getValue() {
        return value;
    }
}
