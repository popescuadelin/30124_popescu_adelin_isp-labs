/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isp.lab9.exercise2;

/**
 *
 * @author Admin
 */
public class Exercise2 {
     public static void main(String[] args) {
         Sensor s1=new Sensor(SENSOR_TYPE.HUMIDITY);
         Sensor s2=new Sensor(SENSOR_TYPE.PRESSURE);
         Sensor s3= new Sensor(SENSOR_TYPE.TEMPERATURE);
         Controller controller=new Controller();
         s1.changeValue(controller);
         s2.changeValue(controller);
         s3.changeValue(controller);
         System.out.println("It works!");
    }
}
