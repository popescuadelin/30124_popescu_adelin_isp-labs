package isp.lab9.exercise2;

import isp.lab9.exercise1.Observer;

public class Controller implements Observer {

    @Override
    public void update(Object event) {
            System.out.println(event.toString());
    }
}
