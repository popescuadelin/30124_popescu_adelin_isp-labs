package isp.lab9.exercise1;

class Alarm extends Observable {
    AlarmController controller = new AlarmController();
    void startAlarm() {
        System.out.println("Alarm has been started!");
        notifyController("SMS");
        this.changeState("START");
    }

    void stopAlarm() {
        System.out.print("Alarm has been stopped!");
        notifyController("SMS");
        this.changeState("STOP");
    }
    void notifyController(Object event){
        controller.update(event);
    }

}